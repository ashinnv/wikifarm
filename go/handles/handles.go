package handles

import (
	"fmt"
	"io/ioutil"
	"math/rand"
	"net/http"
	"os"
	"strings"
	"time"
)

var R404 string = "<html> <h1>404</h1></html>"

func GenServePage() func(w http.ResponseWriter, _ *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {

		var indexHTM []byte = []byte("")
		w.Header().Set("Content-Type", "text/html")

		indexHTM, err := ioutil.ReadFile("./htm/index.html")
		if err != nil {
			fmt.Printf("Issue with reading index file: %s.\nThis is an odd issue and should be investigated.", err)
			w.Write([]byte(R404))
		} else {
			indexHTM = parseHTM(indexHTM)
			w.Write(indexHTM)
		}

	}
}

func GenServeJQ() func(w http.ResponseWriter, _ *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {

		var jqDat []byte = []byte("")
		w.Header().Set("Content-Type", "text/javascript")

		jqDat, err := ioutil.ReadFile("./htm/jquery.js")
		if err != nil {
			fmt.Printf("Issue with reading index file: %s.\nThis is an odd issue and should be investigated.", err)
			w.Write([]byte(R404))
		} else {
			w.Write(jqDat)
		}
	}
}

func GenServeScript() func(w http.ResponseWriter, _ *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {

		var jqDat []byte = []byte("")
		w.Header().Set("Content-Type", "text/javascript")

		jqDat, err := ioutil.ReadFile("./htm/scripts.js")
		if err != nil {
			fmt.Printf("Issue with reading scripts file: %s.\nThis is an odd issue and should be investigated.", err)
			w.Write([]byte(R404))
		} else {
			w.Write(jqDat)
		}

	}
}

func GenServeStyle() func(w http.ResponseWriter, _ *http.Request) {
	return func(w http.ResponseWriter, _ *http.Request) {

		var jqDat []byte = []byte("")

		jqDat, err := ioutil.ReadFile("./htm/styles.css")
		if err != nil {
			fmt.Printf("Issue with reading scripts file: %s.\nThis is an odd issue and should be investigated.", err)
			w.Header().Set("Content-Type", "text/html")
			w.Write([]byte(R404))
		} else {
			//w.Header().Set("Content-Type", "text/css")
			w.Write(jqDat)
		}

	}
}

func GenGetData() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {
		r.ParseForm()
		for key, _ := range r.Form {
			writeToFile("datafile.txt", key)
		}
	}
}

func GenEditDone() func(w http.ResponseWriter, r *http.Request) {
	return func(w http.ResponseWriter, r *http.Request) {

		var output string = ""

		fullText, err := ioutil.ReadFile("datafile.txt")
		if err != nil {
			fmt.Println(err)
		}

		lines := strings.Split(string(fullText), "\n")

		hanz, err := os.OpenFile("datafile.txt", os.O_RDWR|os.O_CREATE|os.O_TRUNC, 0755)
		if err != nil{
			fmt.Println("Error in GenEditDone():", err)
		}
		var trimmed string = ""
		total, _ := ioutil.ReadAll(r.Body)
		trimmed = getStringLine(string(total))

		for count := 0; count < len(lines); count++ {

			fmt.Println("lines: ", lines[count], "trimmed:", trimmed)
			if strings.Contains(trimmed, lines[count]) {
				fmt.Println("FOUND")
				lines[count] = "<div style=\"background-color:red\";><s>" + lines[count] + "</s></div>"
			}
		}

		for _, line := range lines {
			output = output + line + "\n"
			//fmt.Println(line)
		}

		hanz.Truncate(0)
		fmt.Fprintf(hanz, "%s", output)

	}
}

func parseHTM(input []byte) []byte {
	var lines []string
	var tlines string
	var linesb []string

	fDat, err := ioutil.ReadFile("datafile.txt")
	if err != nil {
		fmt.Println("Error reading datafile for html parsing.")
		return input
	}

	repl := ""
	linesb = strings.Split(string(fDat), "\n")

	for _, line := range linesb {
		if !strings.Contains(line, "<s>") {
			tlines = tlines + line + "\n"
		}
	}

	for _, line := range linesb {
		if strings.Contains(line, "<s>") {
			tlines = tlines + line + "\n"
		}
	}

	lines = strings.Split(tlines, "\n")

	for _, line := range lines {

		ranStr := genRandStr()
		lSplit := strings.Split(line, "-:-")

		if len(lSplit) < 2 {
			//fmt.Println("Skipping")
			continue
		}

		lSplit[0] = "<div class=\"time\">" + lSplit[0] + "</div>"
		lSplit[1] = "<div class=\"message\">" + lSplit[1] + "</div>"

		line = lSplit[0] + " " + lSplit[1]
		line = "<div id=\"" + ranStr + "\"  class=\"note\"><h4>" + line + "</h4><button class=\"doneButton\" onclick=\"strike('" + ranStr + "')\">TASK COMPLETED</button></div><hr class=\"gap\">\n"
		repl = repl + line
	}

	fin := strings.Replace(string(input), "<!--Insert List Here-->", repl, 1)
	return []byte(fin)
}

func writeToFile(fileName string, data string) {
	for {
		handle, err := os.OpenFile(fileName, os.O_APPEND|os.O_WRONLY, 0644)
		if err != nil {
			os.Create("datafile.txt")
			continue
		} else {
			tNow := time.Now()
			data = tNow.Format(time.RFC3339) + "-:-" + data + "\n"
			_, err = handle.WriteString(data)
			handle.Close()
			break
		}
	}
}

func genRandStr() string {
	var runes []rune = []rune{'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '1', '2', '3', '4', '5', '6', '7', '8', '9', '0', ' ', '😊', '🔫', '🍆', '👿', '😈', '🤘', '🚓', '🚀', '🌭', '🛫', '🥛', '🥫', '🧯', '🤔', '😥', '💔', '🔥', '🥰'}
	var oStr string = ""

	for iter := 0; iter < 35; iter++ {
		targ := rand.Intn(len(runes)) + 0
		tmp := runes[targ]

		oStr = oStr + string(tmp)
	}

	return oStr
}

//Reference input: <h4><div class="time">2021-02-19T23:28:35-06:00</div> <div class="message">ooga booga</div></h4><button onclick="strike('😥2xq6🥰pf😈i9nla🌭l😈s🔥1🤘o8🥫2🥛🥫🥫fac😥vr🔫')">TASK COMPLETED</button>
func getStringLine(input string) string {

	input = strings.Replace(input, "<h4><div class=\"time\">", "", -1)
	input = strings.Replace(input, "</div> <div class=\"message\">", "-:-", -1)
	fmt.Println("1", input)

	input = strings.Replace(input, "</div></h4><button onclick=\"strike('", "-(-", -1)
	fmt.Println("2", input)

	input = strings.Replace(input, "')\">TASK COMPLETED</button>", "", -1)
	fmt.Println("3", input)

	final := strings.Split(input, "-(-")
	fmt.Println("4", final)

	return final[0]

}
