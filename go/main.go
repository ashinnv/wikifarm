package main

import (
	"flag"
	"net/http"
	"strings"
	"time"

	handles "./handles"
)

func main() {

	var serverPort = flag.String("port", "8080", "Port to serve this on.")
	flag.Parse()

	if strings.Contains(*serverPort, ":") {
		*serverPort = strings.Replace(*serverPort, ":", "", -1)
	}
	startServer(*serverPort)

	for {
		time.Sleep(time.Hour * 1000)
	}
}

func startServer(serverPort string) {

	serveJquery := handles.GenServeJQ()
	serveScript := handles.GenServeScript()
	serveStyle := handles.GenServeStyle()
	servePage := handles.GenServePage()
	getData := handles.GenGetData()
	editf := handles.GenEditDone()

	http.HandleFunc("/", servePage)
	http.HandleFunc("/stk", editf)
	http.HandleFunc("/rec", getData)
	http.HandleFunc("/jq", serveJquery)
	http.HandleFunc("/scr", serveScript)
	http.HandleFunc("/css", serveStyle)

	http.ListenAndServe(":"+serverPort, nil)
}
